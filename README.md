# Certificate Generator PDC
[![Go Reference](https://pkg.go.dev/badge/golang.org/x/build.svg)](https://pkg.go.dev/golang.org/x/build)

Esse microserviço é responsável pela geração de certificados no Parceiro da Construção. Os formatos disponíveis para a geração do certificado é PDF e JPG. Todos os certificados gerados são armazenados no S3. Esse serviço não possuí validação de dados ou validade de certificados, apenas servindo como gerador.

## Linguagens e frameworks
- Golang
- Serverless

## Instructions

- install make
- RUN make build
- ./report

or

- Use docker image (Prefered)

## Deploy

Primeiramente deve-se configurar as credenciais da AWS utilizando o AWS-CLI.

```Shell
npm install -g serverless
make
sls deploy --verbose --profile <your-profile-aws-cli>
```

## Lambda Function
https://u35g2i8n3g.execute-api.us-east-2.amazonaws.com/prd/generate

## Methods
### generate certificate
Responsável por gerar o certificado


``` Shell
curl --location --request POST 'https://u35g2i8n3g.execute-api.us-east-2.amazonaws.com/prd/generate' \
--header 'Content-Type: application/json' \
--data-raw '{
    "Name": "louco",
    "Type": "Curso",
    "EventName": "Meu primeiro curso gerado na API",
    "Date": "10/10/2021",
    "Duration": "10 HORAS",
    "PartnerLogo": "https://diwe-pdc.s3.sa-east-1.amazonaws.com/files/1600101423517.jpg",
    "FilePrefix": "Certificado-Curso-1"
}'
```

##### Instruções:
Quando não houver logo de parceiro, passar o campo como vazio ("")