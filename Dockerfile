FROM golang:1.16-buster AS builder

# Install ImageMagick dev library
RUN apt-get update && apt-get -q -y install \ 
    libmagickwand-dev \ 
    go-dep \
    imagemagick \
    ghostscript

WORKDIR /app
COPY ./report /app

COPY ./report/go.mod ./
COPY ./report/go.sum ./

RUN go mod download

RUN go build -ldflags="-s -w" -tags extlib -o bin/report main.go

# final stage
FROM debian:buster-slim

# Install ImageMagick deps and ca-certificates
RUN apt-get update && apt-get -q -y install \
	ca-certificates \
    imagemagick \
    libmagickwand-dev \
    ghostscript \
	libmagickwand-6.q16-6 && \
	rm -rf /var/lib/apt/lists/*

RUN sed -i '/disable ghostscript format types/,+6d' /etc/ImageMagick-6/policy.xml

COPY --from=builder /app/bin ./

ENTRYPOINT ["./report"]