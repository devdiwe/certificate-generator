package generator

import (
	"encoding/json"
	"image"
	"log"

	"github.com/signintech/gopdf"
	"serverless.go/report/utils"

	"image/color"

	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"gopkg.in/gographics/imagick.v2/imagick"
)

type BodyLabel struct {
	Name        string `json:"Name"`
	Type        string `json:"Type"`
	EventName   string `json:"EventName"`
	Date        string `json:"Date"`
	Duration    string `json:"Duration"`
	PartnerLogo string `json:"PartnerLogo"`
	FilePrefix  string `json:"FilePrefix"`
}
type LabelResponse struct {
	Url     string `example:"url"`
	UrlJpeg string `example:"urlJpeg"`
	Message string `example:"message"`
}

type FilesResponse struct {
	fileJpeg string `example:"fileJpeg"`
	filePdf  string `example:"filePdf"`
}

func GenerateCertificateAction(request events.APIGatewayProxyRequest) (*LabelResponse, error) {

	var bodyLabel BodyLabel

	if err := json.Unmarshal([]byte(request.Body), &bodyLabel); err != nil {
		return nil, err
	}

	generates, err := HandleGeneratorCertificate(bodyLabel)

	print(err)

	region := os.Getenv("AWS_REGION")
	url := "https://" + os.Getenv("AWS_BUCKET") + ".s3." + region + ".amazonaws.com/" + generates.filePdf
	urlJpeg := "https://" + os.Getenv("AWS_BUCKET") + ".s3." + region + ".amazonaws.com/" + generates.fileJpeg
	fmt.Println(url)
	u := &LabelResponse{
		Url:     url,
		UrlJpeg: urlJpeg,
		Message: "Pdf gerado com sucesso!",
	}
	return u, nil
}

func HandleGeneratorCertificate(bodyLabel BodyLabel) (*FilesResponse, error) {

	var err error

	pdf := gopdf.GoPdf{}
	pdf.Start(gopdf.Config{PageSize: gopdf.Rect{W: 841.89, H: 595.28}})
	pdf.AddPage()

	err = pdf.AddTTFFontData("weber-bold", utils.UrlToReader("https://diwe-pdc.s3.sa-east-1.amazonaws.com/Weber-Bold.ttf"))
	err = pdf.AddTTFFontData("weber-light", utils.UrlToReader("https://diwe-pdc.s3.sa-east-1.amazonaws.com/Weber-Light.ttf"))
	if err != nil {
		log.Print(err.Error())
	}

	img := utils.FindFormatFromUrlToJpegImage("https://diwe-pdc.s3-sa-east-1.amazonaws.com/certificado-final-v2.png")

	print("Partner Logo")
	print(bodyLabel.PartnerLogo)
	var logoFormated image.Image = nil

	pdf.ImageFrom(img, 0, 0, &gopdf.Rect{W: 841.89, H: 595.28}) //print image

	if bodyLabel.PartnerLogo != "" {
		print("Entrou aqui")
		logo := utils.FindFormatFromUrlToJpegImage(bodyLabel.PartnerLogo)
		logoFormated = utils.MakeThumbnail(logo, 130, 70, color.RGBA{255, 255, 255, 255})
		print("Thumbnail v2")
		print(logoFormated)
		pdf.ImageFrom(logoFormated, 580, 130, &gopdf.Rect{W: 130, H: 70})
	}

	err = pdf.SetFont("weber-bold", "", 14)
	if err != nil {
		log.Print(err.Error())
	}
	pdf.SetX(95) //move current location
	pdf.SetY(243)
	pdf.Cell(nil, "Nome do Aluno:")

	err = pdf.SetFont("weber-light", "", 17)
	pdf.SetX(95) //move current location
	pdf.SetY(265)
	pdf.Cell(nil, bodyLabel.Name)

	err = pdf.SetFont("weber-bold", "", 14)
	pdf.SetX(95) //move current location
	pdf.SetY(295)
	pdf.Cell(nil, bodyLabel.Type)

	err = pdf.SetFont("weber-light", "", 17)
	pdf.SetX(95) //move current location
	pdf.SetY(318)
	pdf.MultiCell(&gopdf.Rect{
		W: 430,
		H: 50,
	}, bodyLabel.EventName)

	err = pdf.SetFont("weber-bold", "", 14)
	pdf.SetX(540) //move current location
	pdf.SetY(295)
	pdf.Cell(nil, "Concluído em:")

	err = pdf.SetFont("weber-light", "", 14)
	pdf.SetX(540) //move current location
	pdf.SetY(318)
	pdf.Cell(nil, bodyLabel.Date)

	err = pdf.SetFont("weber-bold", "", 14)
	pdf.SetX(660) //move current location
	pdf.SetY(295)
	pdf.Cell(nil, "Duração:")

	err = pdf.SetFont("weber-light", "", 14)
	pdf.SetX(660) //move current location
	pdf.SetY(318)
	pdf.Cell(nil, bodyLabel.Duration)
	print("gerando o arquivo")
	file := pdf.GetBytesPdf()
	filePdf := utils.AddFileToS3(file, ".pdf", bodyLabel.FilePrefix)
	print("Vai gerar imagem agora")

	imagick.Initialize()
	mw := imagick.NewMagickWand()
	err44 := mw.ReadImageBlob(file)
	if err44 != nil {
		fmt.Println(err44)
	}
	mw.SetIteratorIndex(0) // This being the page offset
	mw.SetImageFormat("jpg")
	imageConvert := mw.GetImageBlob()

	fmt.Println(imageConvert)

	fileJpeg := utils.AddFileToS3(imageConvert, ".jpg", bodyLabel.FilePrefix)

	u := &FilesResponse{
		fileJpeg: fileJpeg,
		filePdf:  filePdf,
	}
	return u, nil
}
