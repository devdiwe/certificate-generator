package main

import (
	"fmt"

	"serverless.go/report/handlers"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)


func Handler(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	fmt.Println(req.Path)
	print("here");
	switch req.Path {
		case "/generate":
			return handlers.GenerateCertificate(req)		
		default:
			return handlers.UnhandledMethod()
	}
}

func main() {
	lambda.Start(Handler)
}



