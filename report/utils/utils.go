package utils

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"image/png"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"

	"github.com/disintegration/imaging"
)

func UrlToReader(url string) []byte {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	respBytes, _ := ioutil.ReadAll(resp.Body)
	return respBytes
}

func CheckExtension(url string) string {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(http.DetectContentType(bytes))
	return http.DetectContentType(bytes)
}

func UrlJpegToImage(url string) image.Image {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	respBytes, _ := ioutil.ReadAll(resp.Body)
	img, _ := jpeg.Decode(bytes.NewReader(respBytes))
	if err != nil {
		log.Fatal(err)
	}

	return img
}

func FindFormatFromUrlToJpegImage(url string) image.Image {

	extension := CheckExtension(url)

	if extension != "image/png" {
		return UrlJpegToImage(url)
	}

	pngImgFile := UrlToReader(url)

	// create image from PNG file
	imgSrc, err := png.Decode(bytes.NewReader(pngImgFile))

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// create a new Image with the same dimension of PNG image
	newImg := image.NewRGBA(imgSrc.Bounds())

	draw.Draw(newImg, newImg.Bounds(), &image.Uniform{color.White}, image.Point{}, draw.Src)

	// paste PNG image OVER to newImage
	draw.Draw(newImg, newImg.Bounds(), imgSrc, imgSrc.Bounds().Min, draw.Over)

	// create new out JPEG file
	jpgImgFile := new(bytes.Buffer)

	var opt jpeg.Options
	opt.Quality = 80

	// convert newImage to JPEG encoded byte and save to jpgImgFile
	// with quality = 80
	err = jpeg.Encode(jpgImgFile, newImg, &opt)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	bs := jpgImgFile.Bytes()

	img, _ := jpeg.Decode(bytes.NewReader(bs))

	return img
}

func AddFileToS3(buffer []byte, extension string, prefix string) string {
	// process sending to aws s3
	s, err := session.NewSession(&aws.Config{
		Region: aws.String(os.Getenv("AWS_REGION")),
	})

	if err != nil {
		fmt.Println("erro no login", err)
	}

	fileName := strconv.FormatInt(time.Now().UTC().UnixNano(), 10)
	// Config settings: this is where you choose the bucket, filename, content-type etc.
	// of the file you're uploading.
	output, err := s3.New(s).PutObject(&s3.PutObjectInput{
		Bucket:               aws.String(os.Getenv("AWS_BUCKET")),
		Key:                  aws.String(prefix + fileName + extension),
		ACL:                  aws.String("public-read"),
		Body:                 bytes.NewReader(buffer),
		ContentLength:        aws.Int64(int64(len(buffer))),
		ContentType:          aws.String(http.DetectContentType(buffer)),
		ContentDisposition:   aws.String("attachment"),
		ServerSideEncryption: aws.String("AES256"),
	})
	if err != nil {
		fmt.Println("Erro na hora de inserir a imagem")
		log.Fatal(err)
	}
	if output != nil {
		fmt.Println("Resposta", output)
	}
	return prefix + fileName + extension
}

func MakeThumbnail(img image.Image, w, h int, bgColor color.Color) image.Image {
	return imaging.PasteCenter(
		imaging.New(w, h, bgColor),
		imaging.Fit(img, w, h, imaging.Lanczos),
	)
}
