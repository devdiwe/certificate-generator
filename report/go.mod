module serverless.go/report

go 1.16

require (
	github.com/aws/aws-lambda-go v1.26.0
	github.com/aws/aws-sdk-go v1.40.41
	github.com/disintegration/imaging v1.6.2
	github.com/gen2brain/go-fitz v1.18.0
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/phpdave11/gofpdi v1.0.13 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/signintech/gopdf v0.9.21
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/net v0.0.0-20210908191846-a5e095526f91 // indirect
	golang.org/x/tools/gopls v0.7.2 // indirect
	gopkg.in/gographics/imagick.v2 v2.6.0
)
